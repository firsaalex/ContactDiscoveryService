package org.whispersystems.contactdiscovery;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.whispersystems.signalservice.internal.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ContactDiscoveryClient {

  private static final int MAX_REQUEST_SIZE = 2048;

  public void ping(String url, String authorizationHeader) {
    Response response = ClientBuilder.newClient().target(url).path("/v1/ping").request()
        .header("Authorization", authorizationHeader).put(Entity.json(""));

    System.out.println(response.getStatusInfo());
  }

  public void setRegisteredUser(String url, String authorizationHeader, String user) {
    Response response = ClientBuilder.newClient().target(url).path("/v1/directory/" + user).request()
        .header("Authorization", authorizationHeader).put(Entity.json(""));

    System.out.println(response.getStatusInfo());
  }

  public void removeRegisteredUser(String url, String authorizationHeader, String user) {
    Response response = ClientBuilder.newClient().target(url).path("/v1/directory/" + user).request()
        .header("Authorization", authorizationHeader).delete();

    System.out.println(response.getStatusInfo());
  }

  public void discovery(String url, String authorizationHeader, List<String> contacts, String encryptionKey)
      throws NoSuchAlgorithmException, InvalidKeySpecException, IOException, InvalidKeyException,
      NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

    byte[] key = encryptionKey.getBytes("utf-8");
    DiscoveryRequest dr = new DiscoveryRequest(contacts.size(), encrypt(contacts, key));
    Response response = ClientBuilder.newClient()
                                     .target(url)
                                     .path("/v1/discovery/")
                                     .request(MediaType.APPLICATION_JSON)
                                     .accept(MediaType.APPLICATION_JSON)
                                     .header("Authorization", authorizationHeader)
                                     .put(Entity.json(dr));
    
    System.out.println(response.getStatusInfo());
    DiscoveryResponse dre = response.readEntity(DiscoveryResponse.class);  
    
    System.out.println("Contacts found:");
    for (String s : decrypt(dre.getData(), key)) {
      System.out.println(s);
    }
    
  }

  private List<byte[]> encrypt(List<String> input, byte[] key)
        throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
        BadPaddingException, UnsupportedEncodingException {
    List<byte[]> output = new ArrayList<>();
    SecretKeySpec sks = new SecretKeySpec(key, "AES");

    for (String s : input) {
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.ENCRYPT_MODE, sks);
      output.add(cipher.doFinal(s.getBytes("utf-8")));
    }
    return output;
  }

  private List<String> decrypt(List<byte[]> input, byte[] key)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
      BadPaddingException {
    List<String> output = new ArrayList<>();
    SecretKeySpec sks = new SecretKeySpec(key, "AES");

    for (byte[] b : input) {
      Cipher cipher = Cipher.getInstance("AES");
      cipher.init(Cipher.DECRYPT_MODE, sks);
      output.add(new String(cipher.doFinal(b)));
    }
    return output;
  }

  private static List<String> loadAddressBook(String path) throws IOException {
    List<String>   results = new LinkedList<>();
    BufferedReader reader  = new BufferedReader(new FileReader(new File(path)));

    String line;

    while ((line = reader.readLine()) != null) {
      results.add(line);
    }

    reader.close();
    return results;
  }

  private static void printUsageAndExit(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("ContactDiscoveryClient", options);
    System.exit(0);
  }

  private static CommandLine getCommandLine(String[] argv) throws ParseException {
    Options options = new Options();
    options.addOption("c",  "command",     true, "[register|discover]");
    options.addOption("u",  "username",    true, "Username");
    options.addOption("p",  "password",    true, "Password");
    options.addOption("h",  "host",        true, "Directory service URL");
    options.addOption("s",  "set",         true, "Address to set as registered");
    options.addOption("d",  "delete",      true, "Address to delete from registered");
    options.addOption("a",  "address-file",true, "File containing addresses to lookup");
    options.addOption("k",  "key",         true, "Encryption key");
    options.addOption("S",  "request-size",true, String.format("Maximum number of addresses per discovery request (default %d)", MAX_REQUEST_SIZE));

    options.addOption(null, "signal-host",        true, "Signal service URL");

    CommandLineParser parser      = new DefaultParser();
    CommandLine       commandLine = parser.parse(options, argv);

    if (!commandLine.hasOption("command") ||
        !commandLine.hasOption("host") ||
        !commandLine.hasOption("username") ||
        !commandLine.hasOption("password")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("ContactDiscoveryClient", options);
      System.exit(1);
    }

    String commandType = commandLine.getOptionValue("command");

    if (commandType.equals("register")) {
      if (!commandLine.hasOption("set") &&
          !commandLine.hasOption("delete")) {
        printUsageAndExit(options);
      }
    } else if (commandType.equals("discover")) {
      if (!commandLine.hasOption("address-file") ||
          !commandLine.hasOption("host") || 
          !commandLine.hasOption("key")) {
        printUsageAndExit(options);
      }
    } else {
      printUsageAndExit(options);
    }

    return commandLine;
  }

  private static void handleDiscoverCommand(CommandLine commandLine) throws Throwable {
    String                      username            = commandLine.getOptionValue("username");
    String                      password            = commandLine.getOptionValue("password");
    List<String>                addressBook         = loadAddressBook(commandLine.getOptionValue("address-file"));
    String                      authorizationHeader = "Basic " + Base64.encodeBytes((username + ":" + password).getBytes());
    String                      encryptionKey       = commandLine.getOptionValue("key");
    ContactDiscoveryClient      client              = new ContactDiscoveryClient();
    
    client.discovery(commandLine.getOptionValue("host"), authorizationHeader, addressBook, encryptionKey);    
  }

  private static void handleRegisterCommand(CommandLine commandLine) throws Throwable {
    String                 username            = commandLine.getOptionValue("username");
    String                 password            = commandLine.getOptionValue("password");
    String                 authorizationHeader = "Basic " + Base64.encodeBytes((username + ":" + password).getBytes());
    ContactDiscoveryClient client              = new ContactDiscoveryClient();

    if (commandLine.hasOption("set")) {
      client.setRegisteredUser(commandLine.getOptionValue("host"), authorizationHeader, commandLine.getOptionValue("set"));
    } else {
      client.removeRegisteredUser(commandLine.getOptionValue("host"), authorizationHeader, commandLine.getOptionValue("delete"));
    }
  }

  public static void main(String[] argv) throws Throwable {
    Security.addProvider(new BouncyCastleProvider());

    CommandLine commandLine = getCommandLine(argv);

    if (commandLine.getOptionValue("command").equals("discover")) {
      handleDiscoverCommand(commandLine);
    } else {
      handleRegisterCommand(commandLine);
    }
  }
}
