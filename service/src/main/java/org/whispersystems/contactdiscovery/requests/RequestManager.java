/*
 * Copyright (C) 2017 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.whispersystems.contactdiscovery.requests;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.codahale.metrics.Timer;
import net.openhft.affinity.Affinity;
import net.openhft.affinity.AffinityLock;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.whispersystems.contactdiscovery.directory.DirectoryManager;
import org.whispersystems.contactdiscovery.directory.DirectoryCache;
import org.whispersystems.contactdiscovery.directory.DirectoryUnavailableException;
import org.whispersystems.contactdiscovery.entities.DiscoveryRequest;
import org.whispersystems.contactdiscovery.entities.DiscoveryResponse;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.Tuple;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import io.dropwizard.lifecycle.Managed;
import org.whispersystems.contactdiscovery.util.Constants;

import static com.codahale.metrics.MetricRegistry.name;

public class RequestManager implements Managed {

  private final Logger logger = LoggerFactory.getLogger(RequestManager.class);

  private static final MetricRegistry metricRegistry = SharedMetricRegistries.getOrCreate(Constants.METRICS_NAME);
  private static final Meter processedNumbersMeter = metricRegistry
      .meter(name(RequestManager.class, "processedNumbers"));
  private static final Timer processBatchTimer = metricRegistry.timer(name(RequestManager.class, "processBatch"));
  private static final Histogram batchSizeHistogram = metricRegistry.histogram(name(RequestManager.class, "batchSize"));

  private final DirectoryManager directoryManager;
  private final PendingRequestQueueSet pending;
  private final int TARGET_BATCH_SIZE = 1024;
  private final String encryptionKey;
  private final String key = "KEY";

  public RequestManager(DirectoryManager directoryManager, String encryptionKey) {
    this.directoryManager = directoryManager;
    Map<String, PendingRequestQueue> queueMap = new HashMap<>();
    queueMap.put(key, new PendingRequestQueue());
    this.pending = new PendingRequestQueueSet(queueMap);
    this.encryptionKey = encryptionKey;
  }

  public CompletableFuture<DiscoveryResponse> submit(DiscoveryRequest request) {
    return pending.put(key, request);
  }

  @Override
  public void start() throws Exception {
    int threadCount = AffinityLock.cpuLayout().sockets() * AffinityLock.cpuLayout().coresPerSocket();

    for (int i = 0; i < threadCount; i++) {
      new WorkThread(directoryManager, i).start();
    }
  }

  @Override
  public void stop() throws Exception {

  }

  private class WorkThread extends Thread {

    private final int threadId;
    private final DirectoryManager directoryManager;

    private WorkThread(DirectoryManager directoryManager, int threadId) {
      this.directoryManager = directoryManager;
      this.threadId = threadId;
    }

    @Override
    public void run() {
      try (AffinityLock lock = Affinity.acquireCore()) {
        logger.info(this.getClass().getSimpleName() + " on CPU: " + lock.cpuId());

        for (;;) {
          processBatch(pending.get(TARGET_BATCH_SIZE));
        }
      }
    }

    private void processBatch(List<PendingRequest> requests) {

      List<String> allAddresses = directoryManager.getAddresses();
      byte[] key = new byte[] {};
      try {
        key = encryptionKey.getBytes("utf-8");
      }
      catch (Exception e) {
        e.printStackTrace();
      }

        for (PendingRequest request : requests) {
          try {
            List<String> contacts = decrypt(request.getRequest().getData(), key);
            List<String> result = allAddresses.stream().distinct().filter(contacts::contains).collect(Collectors.toList());
            request.getResponse().complete(new DiscoveryResponse(encrypt(result,key)));
          }
          catch (Exception e) {
            e.printStackTrace();
            request.getResponse().completeExceptionally(e);
          }
        }
      }
    }

    private List<byte[]> encrypt(List<String> input, byte[] key)
        throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
        BadPaddingException, UnsupportedEncodingException {
      List<byte[]> output = new ArrayList<>();
      SecretKeySpec sks = new SecretKeySpec(key, "AES");

      for (String s : input) {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        output.add(cipher.doFinal(s.getBytes("utf-8")));
      }
      return output;
    }

    private List<String> decrypt(List<byte[]> input, byte[] key)
        throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException,
        BadPaddingException {
      List<String> output = new ArrayList<>();
      SecretKeySpec sks = new SecretKeySpec(key, "AES");

      for (byte[] b : input) {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        output.add(new String(cipher.doFinal(b)));
      }
      return output;
    }

}
