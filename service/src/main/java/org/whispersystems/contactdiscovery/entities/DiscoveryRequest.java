/*
 * Copyright (C) 2017 Open Whisper Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.whispersystems.contactdiscovery.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Entity representing an encrypted contact discovery request
 *
 * @author Moxie Marlinspike
 */
public class DiscoveryRequest {

  @JsonProperty
  @NotNull
  private int addressCount;

  @JsonProperty
  @NotNull
  @JsonSerialize()
  @JsonDeserialize()
  private List<byte[]> data;

  public DiscoveryRequest() {

  }

  public DiscoveryRequest(List<byte[]> data) {
    this.addressCount = data.size();
    this.data = data;
  }

  public int getAddressCount() {
    return addressCount;
  }

  public List<byte[]> getData() {
    return data;
  }

  public String toString() {
    return data.toString();
  }

}
